﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace otus.teaching.promocodefactory.api.Controllers
{
    [Route("sample")]
    [Authorize]
    public class SampleController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult("Otus sample...");
        }
    }
}