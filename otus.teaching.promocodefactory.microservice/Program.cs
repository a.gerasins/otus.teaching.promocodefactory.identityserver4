﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace otus.teaching.promocodefactory.microservice
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var client = new HttpClient();

            var disc = await client.GetDiscoveryDocumentAsync("https://localhost:5001");

            if (disc.IsError)
            {
                Console.WriteLine(disc.Error);
                return;
            }

            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disc.TokenEndpoint,
                ClientId = "otus.client",
                ClientSecret = "otus-sample-secret",
                Scope = "scope1"
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            client.SetBearerToken(tokenResponse.AccessToken);

            var response = await client.GetAsync("https://localhost:5011/sample");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
            }
            else
            {
                Console.WriteLine(response.StatusCode);
            }
        }
    }
}